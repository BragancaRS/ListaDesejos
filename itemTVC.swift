//
//  itemTVC.swift
//  WishList
//
//  Created by Raphael Bragança on 7/5/17.
//  Copyright © 2017 Raphael Bragança. All rights reserved.
//
import UIKit

class ItemTVC: UITableViewCell {
   
    
    
    @IBOutlet weak var thumb: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var details: UILabel!
    
    func configureCell(item: Item) {
        title.text = item.title
        price.text = "R$\(item.price)"
        details.text = item.details
        thumb.image = item.toImage?.image as? UIImage
        
    }
    
    
    //
    //    override func awakeFromNib() {
    //        super.awakeFromNib()
    //        // Initialization code
    //    }
    
    
}
