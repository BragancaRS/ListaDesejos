//
//  itemDetailsVC.swift
//  WishList
//
//  Created by Raphael Bragança on 7/5/17.
//  Copyright © 2017 Raphael Bragança. All rights reserved.
//
//


import UIKit
import CoreData

class ItemDetailsVC: UIViewController, UIImagePickerControllerDelegate,UINavigationControllerDelegate, UITextFieldDelegate {
    
    
    @IBOutlet weak var titleField: UITextField!
    @IBOutlet weak var priceField: UITextField!
    @IBOutlet weak var detailsField: UITextField!
    
    
    @IBOutlet weak var tumbImage: UIImageView!
    
    var picker = UIPickerView()
    
    var storeArray = [Store]()
    var itemToEdit: Item?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleField.delegate = self
        priceField.delegate =  self
        detailsField.delegate = self
        
        
        
        if let topItem = self.navigationController?.navigationBar.topItem {
            topItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
        }
        
        
        
        
        if itemToEdit != nil {
            loadItemData()
        }
        
        
        
        // storeField.inputView = picker
        
    }
    
    
    
    
    func loadItemData() {
        
        if let item = itemToEdit {
            
            titleField.text = item.title
            priceField.text = "\(item.price)"
            detailsField.text = item.details
            tumbImage.image = item.toImage?.image as? UIImage
            
            
            
            
        }
    }
    
    
    @IBAction func addImage(_ sender: UIButton) {
        //present(imagePicker, animated: true, completion: nil)
        
        let myPickerImage = UIImagePickerController()
        myPickerImage.delegate = self
        myPickerImage.sourceType = UIImagePickerControllerSourceType.photoLibrary
        myPickerImage.allowsEditing = true
        
        present(myPickerImage, animated: true, completion: nil)
        
        
    }
    
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        tumbImage.image = info[UIImagePickerControllerEditedImage] as? UIImage
        dismiss(animated: true, completion: nil)
        
        
        
        //        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
        //
        //            tumbImage.image = image
        //        }
        //        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    
    
    
    
    
    
    
    @IBAction func deletePressed(_ sender: UIBarButtonItem) {
        if itemToEdit != nil {
            context.delete(itemToEdit!)
            ad.saveContext()
        }
        _ = navigationController?.popViewController(animated: true)
    }
    
    
    
    
    // - Function that maks Insert Method - //
    @IBAction func savePressed(_ sender: UIButton) {
        
        var item: Item!
        
        let picture = Image(context: context)
        picture.image = tumbImage.image    // image tumb
        
        
        if itemToEdit == nil {
            item = Item(context: context)
        } else {
            item = itemToEdit
        }
        
        
        item.toImage = picture
        
        
        
        // Securing Data for Save
        if let title = titleField.text {
            item.title = title
        }
        if let price = priceField.text {
            item.price  = (price as NSString).doubleValue // convert the price init String end finish Double
        }
        if let details = detailsField.text {
            item.details = details
        }
        
        
        ad.saveContext()
        _ = navigationController?.popViewController(animated: true) // return the main Page
    }
    // - Function that maks Insert Method - //
    
    
    
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.titleField.resignFirstResponder()
        self.priceField.resignFirstResponder()
        self.detailsField.resignFirstResponder()
        
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
}
